// Learn more about F# at http://fsharp.org

open System

[<EntryPoint>]
let main argv =
    printfn "Mandelbrot!"

    let resultMandelbrot = MandelBrot.generate 800 600 150.0 32 570
    resultMandelbrot.Save("mandelbrot.bmp")

    printfn "Julia!"
    let resultJulia = Julia.generate 800 600 250.0 2 250
    resultJulia.Save("julia.bmp")

    0 // return an integer exit code
