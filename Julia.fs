module Julia

open System
open System.Numerics
open System.Drawing

let colorMap i nMax =
    let h =
        if ((float (i % 256)) / 255.0) <= 1.0 then
            ((float (i % 256)) / 255.0) * 360.0
        else 360.0
    let s = 0.6
    let v = if i < nMax then 1.0 else 0.0

    MandelBrot.hsbToRgb h s v

let generatePixel x y height width zoom rMax maxIter = 
    let c = MandelBrot.getComplexValueForPixel (float x) (float y) (float height) (float width) zoom
    let z = Complex(-0.8, 0.156)

    let rec findI i c =
        if i >= maxIter then i
        else if Complex.Abs(c) >= (float rMax) then i
        else
            let newRe = c.Real*c.Real - c.Imaginary*c.Imaginary
            let newIm = 2.0 * c.Real * c.Imaginary
            findI (i+1) ((Complex(newRe, newIm))+z)

    colorMap (findI 0 c) maxIter

let generate (width:int) height zoom rMax maxIter =
    let result = new Bitmap(width, height)
    
    for y in 0 .. height-1 do
        for x in 0 .. width-1 do
            generatePixel x y height width zoom rMax maxIter
            |> (fun color -> result.SetPixel(x, y, color))

    result